package az.pasha.hackaton.service;

import az.pasha.hackaton.dto.StepCountDto;
import az.pasha.hackaton.dto.UserDto;
import az.pasha.hackaton.entity.StepCount;
import az.pasha.hackaton.entity.User;
import az.pasha.hackaton.entity.UserCategory;
import az.pasha.hackaton.repository.StepCountRepo;
import az.pasha.hackaton.repository.UserCategoryRepo;
import az.pasha.hackaton.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@CrossOrigin
public class ActivityService {
    private final StepCountRepo stepCountRepo;
    private final UserCategoryRepo userCategoryRepo;
    private final UserRepository userRepository;


    @CrossOrigin

    public StepCountDto getTodayActivity(Long idUser) {
        Optional<User> user= userRepository.findById(idUser);
        if (!user.isPresent())
            throw new IllegalArgumentException("UserNotFound");
        int age=LocalDate.now().getYear()-user.get().getBirthdate().getYear();
        Optional<UserCategory> userCategory = userCategoryRepo.findByFromAgeLessThanEqualAndToAgeGreaterThanEqual(age,age);
        StepCountDto stepCountDto = convertStepCount(stepCountRepo.findByActionDate(LocalDate.now()).get(),userCategory.get());
        return stepCountDto;
    }

    public List<StepCountDto> getHistoryActivity(Long idUser,String fromDate,String toDate) {
        Optional<User> user= userRepository.findById(idUser);
        if (!user.isPresent())
            throw new IllegalArgumentException("UserNotFound");
        int age=LocalDate.now().getYear()-user.get().getBirthdate().getYear();
        Optional<UserCategory> userCategory = userCategoryRepo.findByFromAgeLessThanEqualAndToAgeGreaterThanEqual(age,age);
        List<StepCount> stepCount = stepCountRepo.findAllByActionDateBetween(LocalDate.parse(fromDate),LocalDate.parse(toDate));

        return stepCount.stream().map(stepCount1 -> convertStepCount(stepCount1,userCategory.get())).collect(Collectors.toList());
    }

    public StepCountDto convertStepCount(StepCount s,UserCategory uc){
        return StepCountDto.builder()
                .id(s.getId())
                .actionDate(s.getActionDate())
                .dayOfWeek(s.getActionDate().getDayOfWeek().name())
                .insertDate(s.getInsertDate())
                .userId(convertFromUser(s.getUserId()))
                .activityTime(s.getActivityTime())
                .distance(s.getDistance())
                .stepCount(s.getStepCount())
                .stepCountLimit(uc.getMinStepCount())
                .activityTimeLimit(uc.getMinActivityTime())
                .distanceLimit(uc.getMinDistance().doubleValue())
                .kcalLimit(uc.getMinKcal().doubleValue())
                .kcal(s.getStepCount().doubleValue()/22.9)
                .isDone(s.getStepCount()>uc.getMinStepCount()).build();
    }

    private UserDto convertFromUser(User u) {
        return UserDto.builder()
                .id(u.getId())
                .name(u.getName())
                .birthdate(u.getBirthdate())
                .surname(u.getSurname()).build();
    }

}
