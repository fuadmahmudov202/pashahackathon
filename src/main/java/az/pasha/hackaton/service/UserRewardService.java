package az.pasha.hackaton.service;

import az.pasha.hackaton.dto.RewardDto;
import az.pasha.hackaton.dto.UserRewardDto;
import az.pasha.hackaton.entity.UserReward;
import az.pasha.hackaton.repository.RewardRepo;
import az.pasha.hackaton.repository.UserRepository;
import az.pasha.hackaton.repository.UserRewardRepo;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class UserRewardService {

    private final UserRewardRepo userRewardRepo;
    private final UserRepository userRepository;
    private final RewardRepo rewardRepo;

    public List<UserRewardDto> getUserRewardsById(Long userId) {
        List<UserRewardDto> userRewardDtoList = new ArrayList<>();
        List<UserReward> userRewards = userRewardRepo.findAllByUser(userRepository.findById(userId).orElseThrow(IllegalStateException::new));
        for (UserReward userReward : userRewards) {
            userRewardDtoList.add(
                    UserRewardDto.builder()
                            .categoryName(userReward.getReward().getCategory().getName())
                            .partnerName(userReward.getReward().getPartnerName())
                            .discount(userReward.getReward().getDiscount())
                            .createDate(userReward.getCreateDate())
                            .build()

            );

        }

        return userRewardDtoList;
    }


    public List<RewardDto> getRewards() {

        return rewardRepo.findAll().stream()
                .map(reward -> RewardDto.builder()
                        .partnerName(reward.getPartnerName())
                        .phoneNumber(reward.getPhoneNumber())
                        .discount(reward.getDiscount())
                        .categoryName(reward.getCategory().getName())
                        .build())
                .collect(Collectors.toList());

    }
}
