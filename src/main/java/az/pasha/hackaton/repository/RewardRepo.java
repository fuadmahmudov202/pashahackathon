package az.pasha.hackaton.repository;

import az.pasha.hackaton.entity.Reward;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RewardRepo extends JpaRepository<Reward, Long> {


}
