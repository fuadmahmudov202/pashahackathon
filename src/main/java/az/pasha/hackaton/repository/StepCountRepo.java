package az.pasha.hackaton.repository;

import az.pasha.hackaton.entity.StepCount;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Repository
public interface StepCountRepo extends CrudRepository<StepCount,Long> {
    Optional<StepCount> findByActionDate(LocalDate date);
    List<StepCount> findAllByActionDateBetween(LocalDate fromDate, LocalDate toDate);
}
