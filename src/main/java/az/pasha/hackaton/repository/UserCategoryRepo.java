package az.pasha.hackaton.repository;

import az.pasha.hackaton.entity.UserCategory;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserCategoryRepo extends CrudRepository<UserCategory,Long> {

    @Override
    Optional<UserCategory> findById(Long aLong);

    Optional<UserCategory> findByFromAgeLessThanEqualAndToAgeGreaterThanEqual(Integer age, Integer age2);
}
