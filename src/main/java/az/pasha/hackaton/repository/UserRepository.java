package az.pasha.hackaton.repository;

import az.pasha.hackaton.entity.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.*;

@Repository
public interface UserRepository extends CrudRepository<User,Long> {


}
