package az.pasha.hackaton.repository;

import az.pasha.hackaton.entity.User;
import az.pasha.hackaton.entity.UserCategory;
import az.pasha.hackaton.entity.UserReward;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRewardRepo extends CrudRepository<UserReward, Long> {


    List<UserReward> findAllByUser(User user);
}
