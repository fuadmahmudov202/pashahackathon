package az.pasha.hackaton.entity;

import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table

@FieldDefaults(level = AccessLevel.PRIVATE)
public class UserCategory {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", insertable = false)
    Long id;

    String categoryName;

    Integer fromAge;
    Integer toAge;
    Integer minStepCount;
    Integer minActivityTime;
    Integer minDistance;
    Integer minKcal;


}
