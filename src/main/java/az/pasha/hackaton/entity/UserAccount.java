package az.pasha.hackaton.entity;

import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table
@FieldDefaults(level = AccessLevel.PRIVATE)
public class UserAccount {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", insertable = false)
    Long id;

    @OneToOne
    @JoinColumn(name = "user_id",referencedColumnName = "ID")
    User userId;

    BigDecimal amount;

    LocalDateTime insertDate;




}
