package az.pasha.hackaton.entity;

import lombok.*;
import lombok.experimental.FieldDefaults;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table
@FieldDefaults(level = AccessLevel.PRIVATE)
public class StepCount {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", insertable = false)
    Long id;

    @OneToOne
    @JoinColumn(name = "user_id",referencedColumnName = "ID")
    User userId;
    Integer stepCount;
    Integer activityTime;
    Integer distance;
    LocalDate actionDate;

    @CreationTimestamp
    LocalDateTime insertDate;


}
