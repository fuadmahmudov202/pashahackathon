package az.pasha.hackaton.dto;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDate;

@Data
@Builder
public class UserDto implements Serializable {
    private static final long serialVersionUID = 1234567L;
    Long id;
    String name;
    String surname;
    LocalDate birthdate;
}
