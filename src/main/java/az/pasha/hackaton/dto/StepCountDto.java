package az.pasha.hackaton.dto;

import az.pasha.hackaton.entity.User;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class StepCountDto implements Serializable {
    private static final long serialVersionUID = 1234567L;

    Long id;
    UserDto userId;
    Integer stepCount;
    LocalDate actionDate;
    String dayOfWeek;
    LocalDateTime insertDate;
    Integer activityTime;
    Integer distance;
    Integer stepCountLimit;
    Integer activityTimeLimit;
    Double distanceLimit;
    Double kcalLimit;
    Double kcal;
    Boolean isDone;
}
