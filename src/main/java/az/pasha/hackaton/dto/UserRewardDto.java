package az.pasha.hackaton.dto;

import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@Builder
public class UserRewardDto {

    String partnerName;
    Double discount;
    String categoryName;
    LocalDateTime createDate;


}
