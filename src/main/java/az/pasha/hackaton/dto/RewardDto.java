package az.pasha.hackaton.dto;

import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@Builder
public class RewardDto {

    String partnerName;
    Double discount;
    String categoryName;
    String phoneNumber;


}
