package az.pasha.hackaton.controller;

import az.pasha.hackaton.dto.RewardDto;
import az.pasha.hackaton.dto.UserRewardDto;
import az.pasha.hackaton.service.UserRewardService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RequestMapping("hackaton/user-reward")
@RestController
@RequiredArgsConstructor
@Slf4j
public class UserRewardController {
    private final UserRewardService userRewardService;

    @GetMapping("/")
    public String test() {
        return "ok";
    }
    @GetMapping("/{userId}")
    public List<UserRewardDto> getTodayActivity(@PathVariable Long userId) {
        return userRewardService.getUserRewardsById(userId);
    }

    @GetMapping()
    public List<RewardDto> getRewards() {
        return userRewardService.getRewards();
    }

}
