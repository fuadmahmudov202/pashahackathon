package az.pasha.hackaton.controller;

import az.pasha.hackaton.dto.StepCountDto;
import az.pasha.hackaton.service.ActivityService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RequestMapping("hackaton/activity")
@RestController
@RequiredArgsConstructor
@Slf4j
public class ActivityController {
    private final ActivityService activityService;
    @GetMapping("today/{userId}")
    public StepCountDto getTodayActivity(@PathVariable Long userId){
        return activityService.getTodayActivity(userId);
    }

    @GetMapping("/history/{userId}/{fromDate}/{toDate}")
    public List<StepCountDto> getHistoryActivity(@PathVariable Long userId,@PathVariable String fromDate, @PathVariable String toDate){
        return activityService.getHistoryActivity(userId,fromDate,toDate);
    }

}
